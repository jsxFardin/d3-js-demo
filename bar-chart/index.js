// let dataset = [109, 20, 30, 90, 100, 200, 55, 80, 180, 250, 189];
let dataset = [2, 3, 5, 8, 1];

let svgWidth = 500;
let svgHeight = 300;
let barPadding = 5;

let barWidth = (svgWidth / dataset.length);

let svg = d3.select('svg')
    .attr('height', svgHeight)
    .attr('width', svgWidth);

let yScale = d3.scaleLinear()
    .domain([0, d3.max(dataset)])
    .range([0, svgHeight]);

//  crateing bar
let barChart = svg.selectAll('rect')
    .data(dataset)
    .enter()
    .append('rect')
    .attr('y', (d) => {
        return svgHeight - yScale(d);
    })
    .attr('height', (d) => { return yScale(d) })
    .attr('width', barWidth - barPadding)
    .attr('transform', (d, i) => {
        let translate = [barWidth * i, 0];
        return `translate(${translate})`;
    })
    .attr('class', 'bar');

//  creatiing label for each barchart
// let text = svg.selectAll('text')
//     .data(dataset)
//     .enter()
//     .append('text')
//     .text((d) => { return d })
//     .attr('y', (d, i) => {
//         return svgHeight - d - 2;
//     })
//     .attr('x', (d, i) => {
//         return barWidth * i;
//     })
//     .attr('fill', '#A64C38');
